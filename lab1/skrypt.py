import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import math
import random
import copy

kp = 1.0 # wplyw proporcjonalny
koi = 6.0 # wplyw przeszkod
d0 = 2.0 # rozmiar przeszkod

def distance(x_point, y_point, x, y):
    return np.hypot(x-x_point, y-y_point)

def foi(x_point, y_point, x, y):
	res = distance(x_point, y_point, x, y)
	res[res < d0] = 1

	return 1 * koi * (1/res - 1/d0) * (1/(res**2))

def fp(x_point, y_point, x, y):
	return kp * distance(x_point, y_point, x, y)


delta = 0.01
x_size = 10
y_size = 10

obst_vect = [(0, 5), (8, 3), (-9, 0), (0, 2)]
start_point=(-x_size, -3)
finish_point=(x_size, 5)
x = y = np.arange(-x_size, x_size, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

Z = fp(x_size, 5, X, Y)
for i in obst_vect:
	Z += foi(X, Y, i[0], i[1])

track = np.zeros((x.size, 2))
distance_x = 2*x_size
distance_y = finish_point[1]-start_point[1]
angle = math.tan(distance_y/distance_x)

for index, val in enumerate(np.arange(-x_size, x_size, delta)):
	track[index] = (val, angle * val + 1)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
		   origin='lower', extent=[-x_size, x_size, -y_size, y_size])

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
	plt.plot(obstacle[0], obstacle[1], "or", color='black')

for track in track:
	plt.plot(track[0], track[1], "or", color='pink')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
